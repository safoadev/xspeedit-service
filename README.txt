Document d'installation :

-- Contenu du package
La livraison contient le fichier compressé "xspeedit-service-1.0.0" contenant le fichier "xspeedit-service.jar".

"xspeedit-service.jar" est une application Spring boot et peut être lancer direcetement en standalone.

-- Pré-requis
Le serveur sur lequel doit être lancer le jar doit posséder un JDK avec une version 1.8 minimum.
Commande pour vérifier la version de java : java --version

Le serveur ne doit pas avoir de processus en cours d'execution sur le port 8080.

-- Installation
Pour installation l'application il faut se déplacer dans le répertoire contenant le zip et l'extraire:
cd <EMPLACEMENT_ZIP>
unzip xspeedit-service-1.0.0.zip

-- Exécution
Pour lancer l'application il faut se déplacer dans le répertoire contenant l'application et lancer le jar avec Java :
cd <EMPLACEMENT_JAR>
java -jar xspeedit-service.jar

Vérifier que la ligne de log suivante apparait bien signifiant le bon démarrage de l'application :
2018-07-23 21:27:42.216  INFO 4732 --- [           main] f.x.robot.XspeeditServiceApplication     : Started XspeeditServiceApplication in 4.47 seconds (JVM running for 5.069)


-- Utilisation
Lancer la commande avec en body <CHAINE_ARTICLES> représentant la chaine de caractères des articles à emballer et <IP_SERVEUR> l'adresse IP avec le protocole du serveur.
curl -X POST -s -H "Content-Type: application/json" -d <CHAINE_ARTICLES> <IP_SERVEUR>:8080/robot/chaineArticles

Exemple :
curl -X POST -s -H "Content-Type: application/json" -d '163841689525773' http://localhost:8080/robot/chaineArticles
Sortie : 91/82/73/73/64/55/81/6
