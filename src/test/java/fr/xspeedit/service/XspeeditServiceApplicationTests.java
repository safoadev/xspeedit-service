package fr.xspeedit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.xspeedit.robot.model.Carton;
import fr.xspeedit.robot.service.EmballageArticles;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmballageArticles.class)
public class XspeeditServiceApplicationTests {

	@Autowired
	EmballageArticles ea;

	private void testTailleChaqueCarton(List<Carton> cartons) {
		cartons.stream().forEach(carton -> assertFalse(cartons.size() > 10));
	}

	private void testNombreArticles(String entree, List<Carton> cartons) {
		int nbArticles = (int) cartons.stream().flatMap(x -> x.getArticles().stream()).count();

		assertEquals(entree.length(), nbArticles);
	}

	private List<Carton> testEmballerColis(String entree) {
		List<Carton> cartons = ea.emballerColis(entree);

		testNombreArticles(entree, cartons);
		testTailleChaqueCarton(cartons);

		return cartons;
	}

	private void testConvertirCartonEnString(String entree) {
		List<Carton> cartons = testEmballerColis(entree);
		String result = ea.convertirCartonEnString(cartons);
		assertEquals(result.length() - cartons.size() + 1, entree.length());
	}

	@Test
	public void testEmballerColis1() {
		String entree = "12345";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 2);
	}

	@Test
	public void testEmballerColis2() {
		String entree = "163841689525773";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 8);
	}

	@Test
	public void testEmballerColis5_1() {
		String entree = "5";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 1);
	}

	@Test
	public void testEmballerColis5_2() {
		String entree = "55";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 1);
	}

	@Test
	public void testEmballerColis5_3() {
		String entree = "555";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 2);
	}

	@Test
	public void testEmballerColis1_1() {
		String entree = "1111111111111";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 2);
	}

	@Test
	public void testEmballerColis9_1() {
		String entree = "99999";
		List<Carton> cartons = testEmballerColis(entree);
		assertEquals(cartons.size(), 5);
	}

	@Test
	public void testConvertirCartonEnString1() {
		testConvertirCartonEnString("163841689525773");
	}

	@Test
	public void testConvertirCartonEnString5_1() {
		testConvertirCartonEnString("5");
	}

	@Test
	public void testConvertirCartonEnString5_2() {
		testConvertirCartonEnString("55");
	}

	@Test
	public void testConvertirCartonEnString5_3() {
		testConvertirCartonEnString("555");
	}

	@Test
	public void testConvertirCartonEnString1_1() {
		testConvertirCartonEnString("111111111111");
	}

	@Test
	public void testConvertirCartonEnString5_9() {
		testConvertirCartonEnString("999999999");
	}

}
