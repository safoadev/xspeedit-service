package fr.xspeedit.robot.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import fr.xspeedit.robot.model.Article;
import fr.xspeedit.robot.model.Carton;

/**
 * Service d'emballage des articles
 *
 * @author Safae
 */
@Service
public class EmballageArticles {

	private static final int TAILLE_ARTICLE_MIN = 1;
	private static final int TAILLE_ARTICLE_MAX = 9;

	private static final int TAILLE_CARTON_MAX = 10;

	private Map<Integer, List<Article>> articleParTaille = new HashMap<>();

	private List<Carton> cartons;

	/**
	 * Permet de ranger les articles
	 * 
	 * @param dec
	 * @param inc
	 */
	private void ranger(int dec, int inc) {
		while (!articleParTaille.get(dec).isEmpty()) {
			Article article = articleParTaille.get(dec).get(0);
			articleParTaille.get(dec).remove(article);
			if (!articleParTaille.get(inc).isEmpty()) {
				Article articleBis = articleParTaille.get(inc).get(0);
				ajouterCarton(article, articleBis);
				articleParTaille.get(inc).remove(articleBis);
			} else {
				Carton carton = initialiserCarton(article, dec);
				ajouterAutresArticles(inc, carton);
				cartons.add(carton);
			}
		}

	}

	/**
	 * Permet d'ajouter des articles
	 * 
	 * @param article
	 * @param article2
	 */
	private void ajouterCarton(Article article, Article article2) {
		cartons.add(new Carton(article, article2));
	}

	/**
	 * Permet d'initialiser un carton
	 * 
	 * @param article
	 * @param dec
	 * @return carton
	 */
	private Carton initialiserCarton(Article article, int dec) {
		articleParTaille.get(dec).remove(article);
		return new Carton(article);
	}

	/**
	 * Ajoute des articles tant que la capacité du carton n'est pas atteinte
	 * 
	 * @param inc
	 * @param carton
	 */
	private void ajouterAutresArticles(final int inc, final Carton carton) {
		for (int k = inc; k >= TAILLE_ARTICLE_MIN; k--) {
			// Arrêt de la boucle si carton rempli
			if (carton.getTailleActuelle() == TAILLE_CARTON_MAX) {
				return;
			}
			if (!articleParTaille.get(k).isEmpty() && carton.getTailleActuelle() + k <= TAILLE_CARTON_MAX) {
				Article articleEnCours = articleParTaille.get(k).get(0);
				carton.getArticles().add(articleEnCours);
				articleParTaille.get(k).remove(articleEnCours);
				// Reboucle
				k++;
			}
		}
	}

	/**
	 * converti une chaine d'article en une liste d'article
	 * 
	 * @param chaineArticles
	 * @return List<Article>
	 */
	private List<Article> listeArticles(String chaineArticles) {
		return chaineArticles.chars().boxed().map(x -> {
			return new Article(Integer.parseInt(String.valueOf(Character.toChars(x))));
		}).collect(Collectors.toList());
	}

	/**
	 * trier les articles par taille
	 * 
	 * @param listeArticles
	 * @param i
	 * @return List<Article>
	 */
	private List<Article> trierArticlesParTaille(final List<Article> listeArticles, final int i) {
		return listeArticles.stream().filter(article -> article.getTaille() == i).collect(Collectors.toList());
	}

	/**
	 * Emballer les articles
	 * 
	 * @param chaineArticles
	 * @return List<Carton>
	 */
	public List<Carton> emballerColis(String chaineArticles) {
		cartons = new ArrayList<>();

		List<Article> articles = listeArticles(chaineArticles);

		for (int inc = TAILLE_ARTICLE_MIN; inc <= TAILLE_ARTICLE_MAX; inc++) {
			articleParTaille.put(inc, trierArticlesParTaille(articles, inc));
		}

		for (int dec = TAILLE_ARTICLE_MAX, inc = TAILLE_ARTICLE_MIN; dec > 0; dec--, inc++) {
			ranger(dec, inc);
		}
		return cartons;
	}

	/**
	 * Transforme un objet carton en chaine de caractères
	 * 
	 * @param cartons
	 * @return String
	 */
	public String convertirCartonEnString(List<Carton> cartons) {
		return cartons.stream().sorted((a, b) -> b.getTailleActuelle() - a.getTailleActuelle())
				.map(x -> x.listeArticles()).collect(Collectors.joining("/"));
	}

}
