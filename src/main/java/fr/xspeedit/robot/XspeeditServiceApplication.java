package fr.xspeedit.robot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XspeeditServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(XspeeditServiceApplication.class, args);
	}

}
