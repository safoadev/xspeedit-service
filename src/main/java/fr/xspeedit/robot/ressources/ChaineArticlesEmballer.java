package fr.xspeedit.robot.ressources;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.xspeedit.robot.model.Carton;
import fr.xspeedit.robot.service.EmballageArticles;

/**
 * Ressource de chaine d'emballage des articles
 * 
 * @author Safae
 */
@RestController
public class ChaineArticlesEmballer {

	@Autowired
	private EmballageArticles emballageArticles;

	static final Logger LOG = LoggerFactory.getLogger(ChaineArticlesEmballer.class);

	@PostMapping(path = "/robot/chaineArticles")
	public ResponseEntity<String> articlesEmballer(@RequestBody String chaine) {
		LOG.info("Chaine d'articles en entrée: " + chaine);

		Pattern pattern = Pattern.compile("^\\d+$");
		Matcher matcher = pattern.matcher(chaine);

		if (!matcher.matches()) {
			return new ResponseEntity<String>("La chaine des articles d'entrée ne doit pas contenir de caractères",
					HttpStatus.BAD_REQUEST);
		}

		List<Carton> cartons = emballageArticles.emballerColis(chaine);

		LOG.info("Chaine d'articles en sortie: " + emballageArticles.convertirCartonEnString(cartons));

		return new ResponseEntity<String>(emballageArticles.convertirCartonEnString(cartons), HttpStatus.OK);
	}

}
