package fr.xspeedit.robot.model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Carton {

	private int capacite;
	private List<Article> articles;

	public Carton(Article... articles) {
		super();
		capacite = 10;
		this.articles = Stream.of(articles).collect(Collectors.toList());
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public int getTailleActuelle() {
		return articles.stream().mapToInt(article -> article.getTaille()).sum();
	}

	public String listeArticles() {
		return articles.stream().map(x -> String.valueOf(x.getTaille())).collect(Collectors.joining());
	}

}
