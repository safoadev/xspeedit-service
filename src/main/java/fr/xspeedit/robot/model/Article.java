package fr.xspeedit.robot.model;

public class Article {

	private int taille;

	public Article(int taille) {
		super();
		this.taille = taille;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

}
